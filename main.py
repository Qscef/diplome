import dict
import synt_analiz
import neuron
import re


def main_find():
    print('выберите опцию: 1 - загрузка, 2 - поиск слов')
    option = str(input())
    if option == '1':
        print(create_new_next_words())
    if option == '2':
        print(find_next_words())


def create_new_next_words():
    print('введите тему')
    topic = str(input())
    print('введите новый текст')
    text = str(input())
    # разбиваем на массив предлоджений по знакам препинаний
    split_regex = re.compile(r'[.|!|?|…]')
    sentences = filter(lambda t: t, [t.strip() for t in split_regex.split(text)])
    for sentence in sentences:
        textDict = synt_analiz.create_text_dict(sentence)
        dict.add_new_word(textDict, topic)
    return ('Данные обновились')


def find_next_words():
    # сейчас используем ручной метод выбора темы, потом сделать автоматический через подключение нейросети
    print('Выберите тему - ')
    topic = str(input())
    print('-', topic)
    print('Выберите слово - ')
    word = str(input())
    # возвращение следующих слов
    return get_words(word, topic)
    # добавить разбиение вернувшегося массива на 3 массива разбитые по первому слову, так как возвращается до 9 слов


def get_words(word, topic):
    words = dict.find_next(word, topic)
    return words


def main_neuron():
    print('выберите опцию: 1 - загрузка, 2 - определение темы')
    option = str(input())
    if option == '1':
        print(create_new_neuron())
    if option == '2':
        print('topic = ', return_topic())


def create_new_neuron():
    print('введите тему текста')
    topic = str(input())
    print('введите текст')
    text = str(input())
    text = synt_analiz.create_text_neuron(text)
    for i in range(10):
        neuron.add_neuron(topic, text)
    return 'success'


def return_topic():
    print('введите текст')
    text = str(input())
    text = synt_analiz.create_text_neuron(text)
    return neuron.return_topic(text)


# основная часть
print('выберите опцию: 1 - использование нейросети, 2 - использование функции подстановки слов')
option = str(input())
if option == '1':
    main_neuron()
if option == '2':
    main_find()

