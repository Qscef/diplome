import pymorphy2  # библиотека для привидения слов к нормальной форме


# адрес текста
# filepath = 'txt.txt'
# массив слов (сначало разбивается на предложения, а потом на слова)
# txt_words = []


# функция удаления знаков препинания
def check_sentence(text):
    new_txt = []
    for word in text:
        index = 0
        for char in word:
            if (char == '(') | (char == '"') | (char == '['):
                word = word[1:]
            elif (char == ',') | (char == ':') | (char == '-') | (char == '_') | (char == '!') | (char == '.') | (
                    char == '?') | \
                    (char == '...') | (char == '—') | (char == '"') | (char == "'") | (char == ')') | (char == '–') | (
                    char == '~'):
                word = word[:-1]
            else:
                index += 1
        if word != "":
            new_txt.append(word)
    return new_txt


# функция исправления заглавных букв на прописные
def check_caps(text):
    new_txt = []
    for word in text:
        word = word.lower()
        new_txt.append(word)
    return new_txt


# функция удаления цифр
def check_isdigit(text):
    new_txt = []
    for word in text:
        Number = False
        for char in word:
            if (char == '0') | (char == '1') | (char == '2') | (char == '3') | (char == '4') | (char == '5') | (
                    char == '6') | (char == '7') | (char == '8') | (char == '9'):
                Number = True
        if not Number == True:
            new_txt.append(word)
    return new_txt


# функция превращения всех единиц текста в ормальный вид
def check_normal(text):
    morph = pymorphy2.MorphAnalyzer()
    new_txt = []
    for word in text:
        new_txt.append(morph.parse(word)[0].normal_form)
    return new_txt


def create_text_dict(text):
    # преобразование строки в масссив
    textM = text.split(" ")
    # функция удаления знаков препинания
    textM = check_sentence(textM)
    # функция исправления заглавных букв на прописные
    textM = check_caps(textM)
    # функция удаления цифр
    textM = check_isdigit(textM)
    return textM


def create_text_neuron(text):
    # преобразование строки в масссив
    textM = text.split(" ")
    # функция удаления знаков препинания
    textM = check_sentence(textM)
    # функция исправления заглавных букв на прописные
    textM = check_caps(textM)
    # функция удаления цифр
    textM = check_isdigit(textM)
    # функция перевода слов к нормальной форме
    textM = check_normal(textM)
    return textM


# берем из файла данные по словам и убираем обозначение перехода на новую строчку '/n'
# with open(filepath) as fp:
#   for cnt, line in enumerate(fp):
#     if line:
#       txt_words += (line[:-1]).split(" ")
# # fp = open('txt.txt')
# # for line in fp:
# #  txt_words += (line[:-1]).split(" ")
# fp.close()
# print(txt_words)
# txt_words = check_sentence(txt_words)
# print(txt_words)
# txt_words = check_caps(txt_words)
# print(txt_words)
# txt_words = check_isdigit(txt_words)
# print(txt_words)
# txt_words = check_normal(txt_words)
#  print(txt_words)
