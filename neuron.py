import numpy as np
common_word = ['без', 'безо', 'близ', 'в', 'во', 'вместо', 'вне', 'для', 'до', 'за', 'из', 'изо', 'изза', 'изпод', 'к',
               'ко', 'кроме', 'между', 'меж', 'на', 'над', 'надо', 'о', 'об', 'обо', 'от', 'ото', 'перед', 'пред', 'пo',
               'под', 'подо', 'при', 'про', 'ради', 'с', 'со', 'сквозь', 'среди', 'у', 'через', 'чрез', 'а', 'вдобавок',
               'именно', 'также', 'то', 'благодаря', 'тому', 'что', 'будто', 'вдобавок', 'вследствие', 'чего', 'да',
               'еще', 'и', 'но', 'дабы', 'даже', 'же', 'едва', 'как', 'лишь', 'если', 'затем', 'все', 'следовательно',
               'или', 'кабы', 'скоро', 'будто', 'словно', 'только', 'когда', 'тому', 'того', 'ли', 'между', 'тем',
               'нежели', 'столько', 'не', 'невзирая', 'независимо', 'несмотря', 'однако', 'оттого', 'отчего', 'пока',
               'поскольку', 'потому', 'почему', 'прежде', 'притом', 'причем', 'пускай', 'пусть', 'ради', 'того',
               'чтобы', 'раз', 'раньше', 'чем', 'пор', 'как', 'словно', 'хотя', 'чем', 'что', 'чтоб', 'чтобы', 'я',
               'ты', 'мы', 'он', 'она', 'оно', 'вы', 'они', 'почти', 'по', 'вскоре']


def check_common_word(text):
    result = []
    for word in text:
        repeat = False
        for test in common_word:
            if word == test:
                repeat = True
        if not repeat:
            result.append(word)

    return result


def add_neuron(topic, text):
    text = check_common_word(text)
    if topic == "hockey":
        znachReturn = 1.0
    if topic == "football":
        znachReturn = 0.0
    if topic == "tennis":
        znachReturn = -1.0
    file = open("dictForNeuron.txt", "r")
    nullFile = False
    for line in file:
        if line == '':
            nullFile = True
        if not nullFile:
            temp = []
            i = 1
            exit = False
            while not exit:
                if line[i] == "[":
                    i += 2
                    tempStr = ''
                    tempNumb = ''
                    tempIndex = ''
                    while not line[i] == "'":
                        tempStr += line[i]
                        i += 1
                    if line[i] == "'":
                        i += 3
                    while not line[i] == ",":
                        tempNumb += line[i]
                        i += 1
                    i += 2
                    while not line[i] == "]":
                        tempIndex += line[i]
                        i += 1
                temp.append([tempStr, float(tempNumb), int(tempIndex)])
                if not i+2 == len(line):
                    i += 3
                else:
                    exit = True
    file.close()
    inputNeuronLayer = []
    i = 0
    temp2 = []
    count = len(temp) - 1
    while i <= count:
        inputNeuronLayer.append(0)
        i += 1
    for word in text:
        findOldWord = False
        for oldWord in temp:
            if word == oldWord[0]:
                findOldWord = True
                inputNeuronLayer[oldWord[2]] = 1
        if not findOldWord:
            count += 1
            temp2.append([word, 0, count])
            inputNeuronLayer.append(1)
    temp += temp2
    neuron_list_id = choose_neuron(text, temp)
    temp = init_neuron_weight(neuron_list_id, temp, znachReturn)
    file = open("dictForNeuron.txt", "w")
    file.write(str(temp))
    file.close()


# Сигмоида
def nonlin(x):
    return 1 / (1 + np.exp(-x))


def choose_neuron(text, neuron_all):
    neuron_list_id = []
    for word in text:
        for neuron in neuron_all:
            if (word == neuron[0]):
                neuron_list_id.append(neuron[2])
    return neuron_list_id


def init_neuron_weight(neuron_list_id, neuron_all, znachReturn):
    # входные данные neuron_list_id
    # входные данные znachReturn
    for neuron in neuron_all:
        for neuron_use in neuron_list_id:
            if neuron[2] == neuron_use:
                if neuron[1] != znachReturn:
                    neuron[1] = round(neuron[1]+znachReturn*0.01, 2)
    return neuron_all


def return_topic(text):
    file = open("dictForNeuron.txt", "r")
    nullFile = False
    for line in file:
        temp = []
        i = 1
        exit = False
        while not exit:
            if line[i] == "[":
                i += 2
                tempStr = ''
                tempNumb = ''
                tempIndex = ''
                while not line[i] == "'":
                    tempStr += line[i]
                    i += 1
                if line[i] == "'":
                    i += 3
                while not line[i] == ",":
                    tempNumb += line[i]
                    i += 1
                i += 2
                while not line[i] == "]":
                    tempIndex += line[i]
                    i += 1
            temp.append([tempStr, float(tempNumb), int(tempIndex)])
            if not i+2 == len(line):
                i += 3
            else:
                exit = True
    file.close()
    summ_weight = 0
    koll_neuron = 0
    for word in text:
        for neuron in temp:
            if (word == neuron[0]):
                summ_weight += neuron[1]
                koll_neuron += 1
    if koll_neuron == 0:
        return 0
    elif summ_weight/ koll_neuron > 0.33:
        return 'hockey'
    elif summ_weight/ koll_neuron > -0.33:
        return 'football'
    else:
        return 'tennis'