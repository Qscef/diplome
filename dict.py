class dictionary(object):
    def __init__(self, type):
        self.mDict = []
        self.type = type

    def create_word(self, word, wordNext):
        self.mDict.append([word, [wordNext, [1]]])

    def create_next_word(self, word, wordNext):
        for i in range(len(self.mDict)):
            if self.mDict[i][0] == word:
                self.mDict[i].append([wordNext, [1]])

    def add_frequence(self, word, wordNext):
        for i in range(len(self.mDict)):
            if self.mDict[i][0] == word:
                for j in range(len(self.mDict[i])):
                    if not (j == 0):
                        if self.mDict[i][j][0] == wordNext:
                            self.mDict[i][j][1][0] += 1

    def set_frequence(self, word, wordNext, freq):
        for i in range(len(self.mDict)):
            if self.mDict[i][0] == word:
                for j in range(len(self.mDict[i])):
                    if not (j == 0):
                        if self.mDict[i][j][0] == wordNext:
                            self.mDict[i][j][1][0] = freq

    def get_word_max_frequence(self, word, returnNextWord):
        word_max = ['', '', '']
        freq_max = [0, 0, 0]
        for i in range(len(self.mDict)):
            if self.mDict[i][0] == word:
                for j in range(len(self.mDict[i])):
                    if not (j == 0):
                        if freq_max[0] < freq_max[1]:
                            if freq_max[0] < freq_max[2]:
                                if self.mDict[i][j][1][0] > freq_max[0]:
                                    word_max[0] = self.mDict[i][j][0]
                                    freq_max[0] = self.mDict[i][j][1][0]
                            elif self.mDict[i][j][1][0] > freq_max[2]:
                                word_max[2] = self.mDict[i][j][0]
                                freq_max[2] = self.mDict[i][j][1][0]
                        else:
                            if freq_max[1] < freq_max[2]:
                                if self.mDict[i][j][1][0] > freq_max[1]:
                                    word_max[1] = self.mDict[i][j][0]
                                    freq_max[1] = self.mDict[i][j][1][0]
                            elif self.mDict[i][j][1][0] > freq_max[2]:
                                word_max[2] = self.mDict[i][j][0]
                                freq_max[2] = self.mDict[i][j][1][0]
        # расствим их по популярности
        if freq_max[0] <= freq_max[1]:
            temp = freq_max[0]
            freq_max[0] = freq_max[1]
            freq_max[1] = temp
            temp = word_max[0]
            word_max[0] = word_max[1]
            word_max[1] = temp
        if freq_max[1] <= freq_max[2]:
            temp = freq_max[1]
            freq_max[1] = freq_max[2]
            freq_max[2] = temp
            temp = word_max[1]
            word_max[1] = word_max[2]
            word_max[2] = temp
        if freq_max[0] <= freq_max[1]:
            temp = freq_max[0]
            freq_max[0] = freq_max[1]
            freq_max[1] = temp
            temp = word_max[0]
            word_max[0] = word_max[1]
            word_max[1] = temp
        # первая итерация завершена - поиск 3 самых часто встречаемых слов и получение их частоты
        if not returnNextWord:
            # теперь найдем слова которые следуют за нашими текущими
            temp = []
            temp.append(self.get_word_max_frequence(word_max[0], True))
            temp.append(self.get_word_max_frequence(word_max[1], True))
            temp.append(self.get_word_max_frequence(word_max[2], True))
            result = []
            for i in range(3):
                repeat = False
                result.append([])
                for j in range(3):
                    if (temp[i][j] == '') and (not repeat):
                        repeat = True
                        result[i].append(word_max[i])
                    if not temp[i][j] == '':
                        result[i].append(word_max[i] + ' ' + temp[i][j])
            return result
        else:
            # функцию возврата для 2-го слоя слов
            return word_max

    def check_word(self, word, wordNext):
        if len(self.mDict) == 0:
            self.create_word(word, wordNext)
        else:
            addNew = False
            for i in range(len(self.mDict)):
                if self.mDict[i][0] == word:
                    addNew = True
                    addFreq = False
                    for j in range(len(self.mDict[i])):
                        if not (j == 0):
                            if self.mDict[i][j][0] == wordNext:
                                addFreq = True
                                self.add_frequence(word, wordNext)
                    if not addFreq:
                        self.create_next_word(word, wordNext)
            if not addNew:
                self.create_word(word, wordNext)

    def save_word(self):
        s = ''
        s = str(self.mDict)
        if self.type == 'hockey':
            file = open("hockey.txt", "w")
        if self.type == 'football':
            file = open("football.txt", "w")
        if self.type == 'tennis':
            file = open("tennis.txt", "w")
        file.write(s)
        file.close()

    def load_word(self):
        if self.type == 'hockey':
            file = open("hockey.txt", "r")
        if self.type == 'football':
            file = open("football.txt", "r")
        if self.type == 'tennis':
            file = open("tennis.txt", "r")
        # проверка файла на пустоту
        nullFile = True
        for line in file:
            if not line == '':
                s = line
                nullFile = False
        file.close()
        if not nullFile:
            self.record_word(s)

    def record_word(self, s):
        j = 0
        word = ''
        wordNext = ''
        freq = ''
        i = 0
        while i < len(s):
            if s[i] == '[':
                if j == 0:
                    pass
                else:
                    if j == 1:
                        word = ''
                        i += 2
                        while not (s[i] == "'"):
                            word += s[i]
                            i += 1
                    if j == 2:
                        wordNext = ''
                        i += 2
                        while not (s[i] == "'"):
                            wordNext += s[i]
                            i += 1
                    if j == 3:
                        freq = ''
                        i += 1
                        while not (s[i] == "]"):
                            freq += s[i]
                            i += 1
                        j -= 1
                        self.check_word(word, wordNext)
                        if not(freq == '1'):
                            self.set_frequence(word, wordNext, int(freq))
                j += 1
            else:
                if s[i] == ']':
                    j -= 1
            i += 1

# функция для поиска следующего слова в словаре
def find_next(word, topic):
    if topic == 'hockey':
        # инициализируем словарь, отвечающий за хоккей
        hockey = dictionary('hockey')
        # загружаем словарь
        hockey.load_word()
        return(hockey.get_word_max_frequence(word, False))
    if topic == 'football':
        # инициализируем словарь, отвечающий за футбол
        football = dictionary('football')
        # загружаем словарь
        football.load_word()
        return(football.get_word_max_frequence(word, False))
    if topic == 'tennis':
        # инициализируем словарь, отвечающий за теннис
        tennis = dictionary('tennis')
        # загружаем словарь
        tennis.load_word()
        return(tennis.get_word_max_frequence(word, False))


# функция добавления новых слов
def add_new_word(text, topic):
    #заполняем данными словарь
    if topic == 'hockey':
        # инициализируем словарь отвечающие за хоккей
        hockey = dictionary('hockey')
        # загружаем словарь
        hockey.load_word()
        # заполняем словарь новыми словами
        for i in range(len(text) - 1):
            hockey.check_word(text[i], text[i + 1])
        # сохранение данных в словарь
        hockey.save_word()
    if topic == 'football':
        # инициализируем словарь отвечающие за футбол
        football = dictionary('football')
        # загружаем словарь
        football.load_word()
        # заполняем словарь новыми словами
        for i in range(len(text) - 1):
            football.check_word(text[i], text[i + 1])
        # сохранение данных в словарь
        football.save_word()
    if topic == 'tennis':
        # инициализируем словарь отвечающие за теннис
        tennis = dictionary('tennis')
        # загружаем словарь
        tennis.load_word()
        # заполняем словарь новыми словами
        for i in range(len(text) - 1):
            tennis.check_word(text[i], text[i + 1])
        # сохранение данных в словарь
        tennis.save_word()

# #заполнение тестовыми данными словари
# hockey.check_word('хоккей', 'шайба')
# hockey.check_word('хоккей', 'ворота')
# hockey.check_word('хоккей', 'клюшка')
# hockey.check_word('ворота', 'шайба')
# hockey.check_word('хоккей', 'шайба')
# hockey.check_word('хоккей', 'ворота')
# football.check_word('футбол', 'мяч')
# football.check_word('футбол', 'ворота')
# football.check_word('футбол', 'нога')
# football.check_word('ворота', 'мяч')
# tennis.check_word('теннис', 'мяч')
# tennis.check_word('теннис', 'сетка')
# tennis.check_word('теннис', 'ракетка')
# tennis.check_word('сетка', 'мяч')
# # сохранение данных
# hockey.save_word()
# football.save_word()
# tennis.save_word()


# получение слуующего слова
# text = str(input())
# print(d.get_word_max_frequence(text))


# 1 разработать автоматическое предлоение слов по выбраному словарю -> +
#   1.1 автоматическое предложение следубщего слова
#   1.2 разбить на несколько словарей
# 2 сделать атоматическое определение темы по набранному тексту через нейросеть и доп словари
# ++++ сделать визуализауию как в C#